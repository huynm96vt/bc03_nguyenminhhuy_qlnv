let danhSachNhanVien = [];

let validatorNv = new ValidatorNV();

const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";

function ResetModal() {
  document.querySelector(".sp-thongbao").style.display = "none";

  document.getElementById("modal-content").reset();
}

const timKiemViTri = function (id, array) {
  return array.findIndex(function (nv) {
    return nv.taiKhoan == id;
  });
};

const luuLocalStorage = function () {
  let dsnvJson = JSON.stringify(danhSachNhanVien);

  localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
};

// lấy dữ liệu từ localStorage khi user tải lại trang

var dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);

//  gán cho array gốc và render lại giao diện
if (dsnvJson) {
  danhSachNhanVien = JSON.parse(dsnvJson);

  danhSachNhanVien = danhSachNhanVien.map(function (item) {
    return new NhanVien(
      item.taiKhoan,
      item.tenNv,
      item.emailNv,
      item.matKhau,
      item.ngayLam,
      item.luongcb,
      item.chucVu,
      item.gioLam
    );
  });
  xuatDanhSachNhanVien(danhSachNhanVien);
}

function themNhanVien() {
  let newNhanVien = layThongTinTuForm();

  let isValidTkNv =
    validatorNv.kiemTraRong(
      "tknv",
      "tbTKNV",
      "Tài khoản nhân viên không được để trống"
    ) &&
    validatorNv.kiemTraTrung(newNhanVien, danhSachNhanVien) &&
    validatorNv.kiemTraTkHopLe("tknv", "tbTKNV");

  let isValidTenNv =
    validatorNv.kiemTraRong(
      "name",
      "tbTen",
      "Tên nhân viên không được để trống"
    ) && validatorNv.kiemTraTenHopLe("name", "tbTen");

  let isValidEmail =
    validatorNv.kiemTraRong(
      "email",
      "tbEmail",
      "Email nhân viên không được để trống"
    ) && validatorNv.kiemTraEmail("email", "tbEmail");

  let isValidPassword =
    validatorNv.kiemTraRong(
      "password",
      "tbMatKhau",
      "Mật khẩu không được để trống"
    ) && validatorNv.kiemTraPassword("password", "tbMatKhau");

    let isValidDate = validatorNv.kiemTraRong("datepicker","tbNgay","Ngày làm không được để trống")

    let isValidLuongCb = validatorNv.kiemTraRong("luongCB","tbLuongCB","Lương cơ bản không được để trống") && validatorNv.kiemTraLuong("luongCB","tbLuongCB")

    let isValidChucVu = validatorNv.kiemTraChucVu("chucvu","tbChucVu");

    let isValidHours = validatorNv.kiemTraRong("gioLam","tbGiolam","Giờ làm không được để trống") && validatorNv.kiemTraSoGioLam("gioLam","tbGiolam")
  let isValid = isValidTkNv & isValidTenNv && isValidEmail && isValidPassword && isValidDate && isValidLuongCb && isValidChucVu && isValidHours;
  if (isValid) {
    danhSachNhanVien.push(newNhanVien);
    xuatDanhSachNhanVien(danhSachNhanVien);
    // convert array thành json để có thể lưu vào localStorage
    luuLocalStorage();
    ResetModal();
    $("#myModal").modal("hide");
  }
}

function xoaNhanVien(id) {
  var viTri = timKiemViTri(id, danhSachNhanVien);
  // xoá tại ví trị tìm thấy với số lượng là 1
  danhSachNhanVien.splice(viTri, 1);
  xuatDanhSachNhanVien(danhSachNhanVien);
  luuLocalStorage();
}

function suaNhanVien(id) {
  let viTri = timKiemViTri(id, danhSachNhanVien);

  let nhanVien = danhSachNhanVien[viTri];

  xuatThongTinLenForm(nhanVien);

  $("#myModal").modal("show");
}
function capNhatNhanVien() {
  let nhanVienEdit = layThongTinTuForm();

  let viTri = timKiemViTri(nhanVienEdit.taiKhoan, danhSachNhanVien);

  danhSachNhanVien[viTri] = nhanVienEdit;

  xuatDanhSachNhanVien(danhSachNhanVien);

  luuLocalStorage();

  $("#myModal").modal("hide");
}

function timKiemTheoLoaiNV() {
  let loaiNVTimKIem = document.getElementById("searchName").value;
  let danhSachTheoLoaiNV = danhSachNhanVien.filter(function (item) {
    return item.xepLoaiNV() == loaiNVTimKIem;
  });
  xuatDanhSachNhanVien(danhSachTheoLoaiNV);
}
