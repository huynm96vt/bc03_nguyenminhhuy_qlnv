var NhanVien = function (
  _taiKhoan,
  _ten,
  _email,
  _matKhau,
  _ngayLam,
  _luongcb,
  _chucVu,
  _gioLam
) {
  this.taiKhoan = _taiKhoan;
  this.tenNv = _ten;
  this.emailNv = _email;
  this.matKhau = _matKhau;
  this.ngayLam = _ngayLam;
  this.luongcb = _luongcb;
  this.chucVu = _chucVu;
  this.gioLam = _gioLam;

  this.tinhTongLuong = function () {
    switch (this.chucVu) {
      case "Sếp":
        return (tongLuong = this.luongcb * 3);
      case "Trưởng phòng":
        return (tongLuong = this.luongcb * 2);
      case "Nhân viên":
        return (tongLuong = this.luongcb);
    }
  };

  this.xepLoaiNV = function () {
    if (this.gioLam >= 192) {
      return "Xuất sắc";
    } else if (this.gioLam >= 176) {
      return "Giỏi";
    } else if (this.gioLam >= 160) {
      return "Khá";
    }
    return "Trung bình";
  };
};
