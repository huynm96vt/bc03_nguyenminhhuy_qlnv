function ValidatorNV() {
  this.kiemTraRong = function (idTarget, idError, messageError) {
    var valueTarget = document.getElementById(idTarget).value.trim();
    if (!valueTarget) {
      document.getElementById(idError).innerText = messageError;
      const nodeList = document.querySelectorAll(".sp-thongbao");
      for (let i = 0; i < nodeList.length; i++) {
        nodeList[i].style.display = "block";
      }
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };

  this.kiemTraTrung = function (newNhanVien, danhSachNhanVien) {
    var index = danhSachNhanVien.findIndex(function (item) {
      return item.taiKhoan == newNhanVien.taiKhoan;
    });

    if (index == -1) {
      document.getElementById("tbTKNV").innerText = "";
      return true;
    }
    document.querySelector(".sp-thongbao").style.display = "block";

    document.getElementById("tbTKNV").innerText =
      "Tài khoản nhân viên không được trùng";
    return false;
  };

  this.kiemTraTkHopLe = function (idTarget, idError) {
    let valueInput = document.getElementById(idTarget).value;
    // Kiểm tra input nhập vào phải là số
    if (/^\d+$/.test(valueInput)) {
      if (valueInput.length >= 4 && valueInput.length <= 6) {
        document.getElementById(idTarget).innerText = "";
        return true;
      }
    }
    document.querySelector(".sp-thongbao").style.display = "block";

    document.getElementById(idError).innerText =
      "Tài khoản nhân viên không hợp lệ";
    return false;
  };

  this.kiemTraEmail = function (idTarget, idError) {
    let parten = /^[a-z0-9](.?[a-z0-9]){5,}@g(oogle)?mail.com$/;
    let valueInput = document.getElementById(idTarget).value;

    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.querySelector(".sp-thongbao").style.display = "block";
    document.getElementById(idError).innerText = "Email không hợp lệ";
  };

  this.kiemTraTenHopLe = function (idTarget, idError) {
    let valueInput = document.getElementById(idTarget).value;
    // Kiểm tra input nhập vào phải là chữ
    if (/^[A-Za-z]+$/.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.querySelector(".sp-thongbao").style.display = "block";
    document.getElementById(idError).innerText = "Tên không hợp lệ";
  };

  this.kiemTraPassword = function (idTarget, idError) {
    let valueInput = document.getElementById(idTarget).value;
    // Kiểm tra input nhập vào từ 6-10 ký tự chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt
    if (
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]/.test(
        valueInput
      ) &&
      valueInput.length <= 10 &&
      valueInput.length >= 6
    ) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Mật khẩu không hợp lệ";
    return false;
  };
  
  this.kiemTraLuong = function(idTarget,idError){
    let luongCb = document.getElementById(idTarget).value*1;
    if((luongCb >= 1000000) && (luongCb <= 20000000)){
      document.getElementById(idError).innerText = "";
      return true;
    }

    document.getElementById(idError).innerText = "Nhập lại lương cơ bản";
    return false;
  }

  this.kiemTraChucVu = function(idTarget,idError){
    let valueSelected = document.getElementById(idTarget).value;
    if(valueSelected == "default"){
      document.getElementById(idError).innerText = "Phải chọn chức vụ";
      return false;
    }
    document.getElementById(idError).innerText = "";
    return true;
  }

  this.kiemTraSoGioLam = function(idTarget,idError){
    let soGioLam = document.getElementById(idTarget).value*1;
    if((soGioLam >= 80) && (soGioLam <= 200)){
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Nhập lại số giờ làm";
    return false;
  }
}
