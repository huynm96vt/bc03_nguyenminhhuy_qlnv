function layThongTinTuForm() {
  let tkNv = document.getElementById("tknv").value;
  let tenNv = document.getElementById("name").value;
  let emailNv = document.getElementById("email").value;
  let mkNv = document.getElementById("password").value;
  let ngayLam = document.getElementById("datepicker").value;
  let luongCB = document.getElementById("luongCB").value;
  let chucVu = document.getElementById("chucvu").value;
  let gioLam = document.getElementById("gioLam").value *1;

  return nhanVien = new NhanVien(tkNv, tenNv, emailNv, mkNv, ngayLam, luongCB, chucVu, gioLam);

}

function xuatDanhSachNhanVien(dsnv) {
  let contentHTML = "";

  for (let index = 0; index < dsnv.length; index++) {
    let nhanVien = dsnv[index];

    let contentTrTag = /*html*/ `<tr>
    <td>${nhanVien.taiKhoan}</td>
    <td>${nhanVien.tenNv}</td>
    <td>${nhanVien.emailNv}</td>
    <td>${nhanVien.ngayLam}</td>
    <td>${nhanVien.chucVu}</td>
    <td>${nhanVien.tinhTongLuong()}</td>
    <td>${nhanVien.xepLoaiNV()}</td>
    <td>  
    <button
    onclick="suaNhanVien('${nhanVien.taiKhoan}')"
    class="btn btn-success">Sửa</button>
    <button class="btn btn-danger"  onclick="xoaNhanVien('${nhanVien.taiKhoan}')">Xoá</button>
    </td>
                          </tr>`;

    contentHTML += contentTrTag;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function xuatThongTinLenForm(nv) {
document.getElementById("tknv").value = nv.taiKhoan;
document.getElementById("name").value = nv.tenNv;
document.getElementById("email").value = nv.emailNv;
document.getElementById("password").value = nv.matKhau;
document.getElementById("datepicker").value = nv.ngayLam;
document.getElementById("luongCB").value = nv.luongcb;
document.getElementById("chucvu").value = nv.chucVu;
document.getElementById("gioLam").value = nv.gioLam
}
